import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import { InMemoryDataService } from './HttpServices/in-memory-data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactoService } from './services/contacto.Service';
import { AgendaComponent } from './agenda/agenda.component';
import { ContactoComponent } from './contacto/contacto.component';
import { AgregarContactoComponent } from './agregar-contacto/agregar-contacto.component';
import { ContactofilterPipe } from './contactofilter.pipe';
@NgModule({
  declarations: [
    AppComponent,
   
   
    AgendaComponent,
    ContactoComponent,
    AgregarContactoComponent,
    ContactofilterPipe,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService,{dataEncapsulation:false}
    )
    
  ],
  providers: [ContactoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
