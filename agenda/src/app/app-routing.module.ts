import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendaComponent } from './agenda/agenda.component';
import { ContactoComponent } from './contacto/contacto.component';
import { AgregarContactoComponent } from './agregar-contacto/agregar-contacto.component';

const routes: Routes = [
  {path:'agenda',component:AgendaComponent},
  {path:'contacto',component:ContactoComponent},
  {path:'',redirectTo:'/agenda',pathMatch:'full'},
  {path:'detalle/:id',component:ContactoComponent},
  {path:'agregar',component:AgregarContactoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
