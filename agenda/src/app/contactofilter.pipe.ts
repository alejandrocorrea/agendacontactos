import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contactofilter'
})
export class ContactofilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
   if(!args){
     return value;
   }

 return value.filter(items=>{
    return items.nombres.startsWith(args)==true
   })
  }

}
