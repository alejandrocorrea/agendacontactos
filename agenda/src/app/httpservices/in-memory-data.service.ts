import {  InMemoryDbService} from "angular-in-memory-web-api";

export class InMemoryDataService implements InMemoryDbService{
    createDb(){
        const contactos=[
           { id:0,nombres:"juan camilo",apellidos:"velez torres",lTrabajo:"casa",numFijo:12334567,numMovil:345457563,direccion:"cra 10 23n-12",email:"elvelez@gmail.com"},
           { id:1,nombres:"juana camila",apellidos:"velez torres",lTrabajo:"casa",numFijo:12334567,numMovil:345457563,direccion:"cra 10 23n-12",email:"elvelez@gmail.com"},
           { id:2,nombres:"ana lisa",apellidos:"meste golondrino",lTrabajo:"casa",numFijo:12334567,numMovil:345457563,direccion:"cra 10 23n-12",email:"elvelez@gmail.com"},
        ]
        return {contactos};
    }
}