import { Component, OnInit, Input } from '@angular/core';
import { Contacto } from '../objects/contacto';
import { ContactoService } from '../services/contacto.Service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from "@angular/common";
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
@Input() contacto:Contacto;

  constructor(private contactoService:ContactoService,
    private route:ActivatedRoute,
    private location:Location) { }

  ngOnInit() 
  {
    this.route.params.forEach((params:Params)=>{
      if(params['id'] !== undefined){
        let id=+params['id'];
        this.contactoService.getContacto(id).subscribe(contacto=>this.contacto=contacto)
      }
    })
  }
  guardar():void{
    this.contactoService.actualizar(this.contacto).subscribe(()=>this.goBack())
  }
  goBack(): void
{
  this.location.back();
}

}
