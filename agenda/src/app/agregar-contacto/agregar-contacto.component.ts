import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Contacto } from '../objects/contacto';
import { ContactoService } from '../services/contacto.Service';

@Component({
  selector: 'app-agregar-contacto',
  templateUrl: './agregar-contacto.component.html',
  styleUrls: ['./agregar-contacto.component.css']
})
export class AgregarContactoComponent implements OnInit {
  form: FormGroup;
  constructor(
    private contactosService: ContactoService,
    private location: Location,
    public fb: FormBuilder) {
    this.form = this.fb.group({
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      lTrabajo: ['', [Validators.required]],
      numFijo: ['', [Validators.required]],
      numMovil: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]]
    })
  }

  contactos: Contacto[]
  ngOnInit() {
  }
  agregar(): void {
    
      this.contactosService.crearNuevo(this.form.value).subscribe();
      this.limpiarForm()
    
  }
  limpiarForm(){
    this.form=this.fb.group({
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      lTrabajo: ['', [Validators.required]],
      numFijo: ['', [Validators.required]],
      numMovil: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email,  this.emailDomainValidator]]
    })
      
  }
  emailDomainValidator(control: FormControl) { (1)
    let email = control.value; (2)
    if (email && email.indexOf("@") != -1) { (3)
      let [_, domain] = email.split("@"); (4)
      if (domain !== "(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i") { (5)
        return {
          emailDomain: {
            parsedDomain: domain
          }
        }
      }
    }
    return null; (6)
  }
  goBack(): void {
    this.location.back();
  }
}