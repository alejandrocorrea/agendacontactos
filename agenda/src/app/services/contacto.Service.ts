import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from "rxjs";
import { catchError,map,tap } from "rxjs/operators";
import {Contacto} from '../objects/contacto'

const httpOptions={
    headers: new HttpHeaders({'content-type':'application/json'})
}
@Injectable()
export class ContactoService
{
    private contactosUrl='api/contactos';
    constructor(private http:HttpClient){}
 private log(message:string){
     console.log(`ContactoService:${message}`)
 }
    getContactos():Observable <Contacto[]>{
    // return Promise.resolve(BANDAS)
    return this.http.get<Contacto[]>(this.contactosUrl).pipe(
        tap(bandas=>this.log('contactos almacenados')),catchError(this.handleError('getContactos',[]))
    )
    }
    private handleError<T>(operation='operation',result?:T){
        return(error:any):Observable<T>=>{
            console.error(error);
            this.log(`${operation} failed:${error.message}`);
            return of(result as T)
        }
    }
    getContacto(id:number) : Observable<Contacto>
{
    const url=`${this.contactosUrl}/${id}`;
 return this.http.get<Contacto>(url).pipe(
     tap(_=>this.log(`contacto almacenado id=${id}`),
     catchError(this.handleError<Contacto>(`getContacto id=${id}`)))
 )
}
actualizar (contacto:Contacto):Observable<any>{
    return this.http.put(this.contactosUrl,contacto, httpOptions).pipe(
        tap(_=> this.log(`contacto actualizado id=${contacto.id}`)),
        catchError(this.handleError<any>('actualizar'))
    )
}
 
 crearNuevo (contacto:Contacto):Observable<Contacto>{
     return this.http.post<Contacto>(this.contactosUrl,contacto,httpOptions).pipe(
         tap((contacto:Contacto)=> this.log(`Nuevo contacto w/ id=${contacto.id}`)),
         catchError(this.handleError<Contacto>('crearnuevo'))
     )
 }
 borrar(contacto:Contacto | number):Observable<Contacto>{
     const id=typeof contacto ==='number'? contacto:contacto.id;
     const url=`${this.contactosUrl}/${id}`;
     return this.http.delete<Contacto>(url,httpOptions).pipe(
         tap(_=> this.log(`banda borrada id=${id}`)),
         catchError(this.handleError<Contacto>('borrar'))
     )
 }

}