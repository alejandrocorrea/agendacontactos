import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../services/contacto.Service';
import { Contacto } from '../objects/contacto';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
 contactos:Contacto[]=[]
 nombres:string=""
constructor(private contactosService:ContactoService) { }

  ngOnInit() 
  {
    
    this.contactosService.getContactos().subscribe(contactos=>this.contactos=contactos)
   
  }
  borrar(contacto:Contacto):void
{
  this.contactos=this.contactos.filter(h=>h !== contacto)
  this.contactosService.borrar(contacto).subscribe(()=>this.ngOnInit())
}


}
