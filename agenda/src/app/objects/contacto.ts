export class Contacto{
    id:number;
    nombres:string;
    apellidos:string;
    lTrabajo:string;
    numFijo:number;
    numMovil:number;
    direccion:string;
    email:string;
}